package news;

import java.util.Calendar;



// class for holding all the story info
public class NewsInfo {
	
	public enum NewsCategory
	{
		UNDEFINED(-1),
		MLB(1),
		NFL(2),
		NCAAFB(3),
		NCAAMBB(4),
		NBA(5),
		NHL(6),
		SOCCER(7),
		OTHER(100);
		
		private final int value;
		private NewsCategory(int ord)
		{
			this.value = ord;
		}
		
		public int getValue()
		{
			return value;
		}
	}
	
	// Variables
	private int id = 0;
	private String title = "";
	private String description = "";
	private String link = "";
	private String author = "";
	private String sourceLink = "";
	private int clicks = 0;
	private int category = -1;
	private String thumbnail = "";
	private Calendar timestamp = null;
	private String keywords = "";
	/**
	 * @param id
	 * @param title
	 * @param descriptio
	 * @param link
	 * @param author
	 * @param sourceLink
	 * @param clicks
	 * @param category
	 * @param thumpnail
	 * @param timestamp
	 */
	public NewsInfo(int id, String title, String description, String link,
			String author, String sourceLink, int clicks, int category,
			String thumbnail, Calendar timestamp) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.link = link;
		this.author = author;
		this.sourceLink = sourceLink;
		this.clicks = clicks;
		this.category = category;
		this.thumbnail = thumbnail;
		this.timestamp = timestamp;
	}
	
	public NewsInfo(int id, String title, String description, String link,
			String author, String sourceLink, int clicks, int category,
			String thumbnail, Calendar timestamp, String keywords) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.link = link;
		this.author = author;
		this.sourceLink = sourceLink;
		this.clicks = clicks;
		this.category = category;
		this.thumbnail = thumbnail;
		this.timestamp = timestamp;
		this.setKeywords(keywords);
	}
	
	public NewsInfo(String title, String description, String link,
			String author, String sourceLink, int clicks, int category,
			String thumbnail) {
		super();
		this.title = title;
		this.description = description;
		this.link = link;
		this.author = author;
		this.sourceLink = sourceLink;
		this.clicks = clicks;
		this.category = category;
		this.thumbnail = thumbnail;
	}
	
	public NewsInfo(String title, String description, String link,
			String author, String sourceLink, int clicks, int category,
			String thumbnail, String keywords) {
		super();
		this.title = title;
		this.description = description;
		this.link = link;
		this.author = author;
		this.sourceLink = sourceLink;
		this.clicks = clicks;
		this.category = category;
		this.thumbnail = thumbnail;
		this.setKeywords(keywords);
	}
	
	public NewsInfo() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSourceLink() {
		return sourceLink;
	}
	public void setSourceLink(String sourceLink) {
		this.sourceLink = sourceLink;
	}
	public int getClicks() {
		return clicks;
	}
	public void setClicks(int clicks) {
		this.clicks = clicks;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public Calendar getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Calendar timestamp) {
		this.timestamp = timestamp;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	// overrides
	@Override
	public String toString() {
		return "NewsInfo [id=" + id + ", title=" + title + ", description="
				+ description + ", link=" + link + ", author=" + author
				+ ", sourceLink=" + sourceLink + ", clicks=" + clicks
				+ ", category=" + category + ", thumbnail=" + thumbnail
				+ ", timestamp=" + timestamp + ", keywords=" + keywords + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((link == null) ? 0 : link.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsInfo other = (NewsInfo) obj;
		if (link == null) {
			if (other.link != null)
				return false;
		} else if (!link.equals(other.link))
			return false;
		return true;
	}
	
	

	
	
}
