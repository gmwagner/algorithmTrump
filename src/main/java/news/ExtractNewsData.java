package news;

import java.net.URL;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class ExtractNewsData {
	//The path of the folder that you want to save the images to
	    private static final String folderPath = "<FOLDER PATH>";
	    
	    public static String getThumbnail(String webSiteURL)
	    {
	    	String returnString = "";
	    	Document doc;
			
			doc = utilities.Crawl.getDocument(webSiteURL);
			for(Element meta : doc.select("meta")) {
    		    //System.out.println("Name: " + meta.attr("property") + " - Content: " + meta.attr("content"));
    		    if(meta.attr("property").equals("og:image"))
    		    {
    		    	return meta.attr("content");
    		    }
    		}
	    	
	    	
	    	return returnString;
	    }
	    
	    public static void parsePage(String webSiteURL) {
	 
	        try {
	 
	            //Connect to the website and get the html
	            Document doc = utilities.Crawl.getDocument(webSiteURL);
	            //Get all elements with img tag ,
	            Elements img = doc.getElementsByTag("img");
	            for (Element el : img) {
	                //for each element get the srs url
	                String src = el.absUrl("src");
	                System.out.println("Image Found!");
	                System.out.println("src attribute is : "+src);
	                getImages(src);
	            }
	        } catch (IOException ex) {
	            System.err.println("There was an error");
	        }
	    }
	    private static void getImages(String src) throws IOException {
	        //Exctract the name of the image from the src attribute
	        int indexname = src.lastIndexOf("/");
	        if (indexname == src.length()) {
	            src = src.substring(1, indexname);
	        }
	        indexname = src.lastIndexOf("/");
	        String name = src.substring(indexname, src.length());
	        System.out.println(name);
	        //Open a URL Stream
	        URL url = new URL(src);
	        InputStream in = url.openStream();

	        OutputStream out = new BufferedOutputStream(new FileOutputStream( folderPath+ name));
	        for (int b; (b = in.read()) != -1;) {
	            out.write(b);
	        }
	        out.close();
	        in.close();
	    }
}
