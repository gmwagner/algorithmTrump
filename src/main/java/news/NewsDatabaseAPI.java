package news;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewsDatabaseAPI {
	
	// adds rss story to database it will not accept values with the same link
	//
	// input: db connection and newInfo class val
	// output: none
	//
	public static void addStory(Connection con, NewsInfo newsInfo)
	{
		ArrayList<NewsInfo> newsArray = new ArrayList<NewsInfo>();
		searchByLink(con, newsInfo.getLink(),newsArray);
		if(newsArray.size() == 0)
		{
			String insertStatement = "INSERT INTO "
					+ "news(title,description,link,author,sourcelink,clicks,category,thumbnail,keywords) "
					+ "VALUES(?,?,?,?,?,?,?,?,?)";
			PreparedStatement pstmt = null;
			try
			{
				pstmt = con.prepareStatement(insertStatement);
				pstmt.setString(1, newsInfo.getTitle());
				pstmt.setString(2, newsInfo.getDescription());
				pstmt.setString(3, newsInfo.getLink());
				pstmt.setString(4, newsInfo.getAuthor());
				pstmt.setString(5, newsInfo.getSourceLink());
				pstmt.setInt(6, newsInfo.getClicks());
				pstmt.setInt(7, newsInfo.getCategory());
				pstmt.setString(8, newsInfo.getThumbnail());
				pstmt.setString(9, newsInfo.getKeywords().toLowerCase());
				pstmt.executeUpdate();
				
			}
			catch (SQLException ex) {
		         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
		         lgr.log(Level.SEVERE, ex.getMessage(), ex);
			}
			finally
			{
				try{
		        	if(pstmt != null)
		        	{
		        		pstmt.close();
		        	}
	        	}
	    	    catch (SQLException ex) {
	    	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
	    		}
			}
		}

	}

	// Retrieve items limit
	//
	// Displays items all from limit
	//
	public static void retrieveNewsLimit(
			Connection con, 
			ArrayList<NewsInfo> newsArray,
			int startLimit,
			int endLimit,
			String keywords
	)
	{
		int limit = endLimit - startLimit;
        String statement = "SELECT * FROM news where keywords LIKE ? ORDER BY timestamp DESC LIMIT ? OFFSET ?;";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try
		{
			
			pstmt = con.prepareStatement(statement);
			pstmt.setString(1, "%" + keywords + "%");
			pstmt.setInt(2, limit);
			pstmt.setInt(3, startLimit);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Date date = rs.getDate(9);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
	            newsArray.add(new NewsInfo(rs.getInt(1), rs.getString(2), 
	            		rs.getString(3), rs.getString(4), rs.getString(5), 
	            		rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(10),cal));
			}
			
		}
		catch (SQLException ex) {
	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	    finally
	    {
	        	
	       	try{
		        	if(pstmt != null)
		        	{
		        		pstmt.close();
		        	}
		        	if(rs != null)
		        	{
		        		rs.close();
		        	}
	       	}
	   	    catch (SQLException ex) {
	   	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	   	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
	   		}
	    }
		
	} // end retrieveNewsLimit
	
	// Retrieve items limit
	//
	// Displays items all from limit
	//
	public static void retrieveNewsLimit(
			Connection con, 
			ArrayList<NewsInfo> newsArray,
			int startLimit,
			int endLimit,
			int category
	)
	{
		int limit = endLimit - startLimit;
        String statement = "SELECT * FROM news WHERE category = ? ORDER BY timestamp DESC LIMIT ? OFFSET ?;";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try
		{
			
			pstmt = con.prepareStatement(statement);
			pstmt.setInt(1, category);
			pstmt.setInt(2, limit);
			pstmt.setInt(3, startLimit);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Date date = rs.getDate(9);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
	            newsArray.add(new NewsInfo(rs.getInt(1), rs.getString(2), 
	            		rs.getString(3), rs.getString(4), rs.getString(5), 
	            		rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(10),cal));
			}
			
		}
		catch (SQLException ex) {
	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	    finally
	    {
	        	
	       	try{
		        	if(pstmt != null)
		        	{
		        		pstmt.close();
		        	}
		        	if(rs != null)
		        	{
		        		rs.close();
		        	}
	       	}
	   	    catch (SQLException ex) {
	   	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	   	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
	   		}
	    }
		
	} // end retrieveNewsLimit
	
	// Retrieve items limit
	//
	// Displays items all from limit
	//
	public static void retrieveNewsLimit(
			Connection con, 
			ArrayList<NewsInfo> newsArray,
			int startLimit,
			int endLimit
	)
	{
		int limit = endLimit - startLimit;
        String statement = "SELECT * FROM news ORDER BY timestamp DESC LIMIT ? OFFSET ?";
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        
        try
		{
			
			pstmt = con.prepareStatement(statement);
			pstmt.setInt(1, limit);
			pstmt.setInt(2, startLimit);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Date date = rs.getDate(9);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
	            newsArray.add(new NewsInfo(rs.getInt(1), rs.getString(2), 
	            		rs.getString(3), rs.getString(4), rs.getString(5), 
	            		rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(10),cal));
			}
			
		}
		catch (SQLException ex) {
	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	    finally
	    {
	        	
	       	try{
		        	if(pstmt != null)
		        	{
		        		pstmt.close();
		        	}
		        	if(rs != null)
		        	{
		        		rs.close();
		        	}
	       	}
	   	    catch (SQLException ex) {
	   	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	   	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
	   		}
	    }
		
	} // end retrieveNewsLimit
	
	// extracts all feeds in the last X number of hours
		public static void retrieveInLastHours(Connection con, ArrayList<NewsInfo> newsArray, int hoursBack)
		{
			String queryStatement = String.format("SELECT * FROM news where timestamp::time < (now() - INTERVAL '%d hour')::time ORDER BY timestamp desc;", hoursBack);
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try
			{
				
				pstmt = con.prepareStatement(queryStatement);
				//pstmt.setInt(1, hoursBack);
				rs = pstmt.executeQuery();
				while (rs.next()) {
					Date date = rs.getDate(9);
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);
		            newsArray.add(new NewsInfo(rs.getInt(1), rs.getString(2), 
		            		rs.getString(3), rs.getString(4), rs.getString(5), 
		            		rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(10),cal));
				}
				
			}
			catch (SQLException ex) {
		         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
		         lgr.log(Level.SEVERE, ex.getMessage(), ex);
			}
		    finally
		    {
		        	
		       	try{
			        	if(pstmt != null)
			        	{
			        		pstmt.close();
			        	}
			        	if(rs != null)
			        	{
			        		rs.close();
			        	}
		       	}
		   	    catch (SQLException ex) {
		   	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
		   	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		   		}
		    }
		} // retrieveInLastHours

	public static void searchByLink(Connection con, String link, ArrayList<NewsInfo> newsArray) {
		String selectStatement = String.format("SELECT * FROM news WHERE link LIKE ?");
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = con.prepareStatement(selectStatement);
			pstmt.setString(1, link);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Date date = rs.getDate(9);
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
	            newsArray.add(new NewsInfo(rs.getInt(1), rs.getString(2), 
	            		rs.getString(3), rs.getString(4), rs.getString(5), 
	            		rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getString(10),cal));
			}
			
		}
	    catch (SQLException ex) {
	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	    finally
	    {
	        	
        	try{
	        	if(pstmt != null)
	        	{
	        		pstmt.close();
	        	}
	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(NewsDatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
	    }
		
	}
	
}
