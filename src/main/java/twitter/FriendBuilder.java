package twitter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import machineLearning.SentimentAnalysis;
import snoozle.algorithmTrump.SentimentString;
import twitter4j.IDs;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import utilities.TwitterUtilities;
import utilities.TwitterUtilities.TwitterAccounts;

public class FriendBuilder {
	
	private static final int maxDelay = 5;
	private static final int interval = 15;
	private static Random random = new Random(Calendar.getInstance().getTimeInMillis());
	private static final int sleepStart = 21; // 9:00PM
	private static final int sleepStop = 9; // 9:00 AM
	private static SimpleDateFormat hourSDF = new SimpleDateFormat("kk");
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd kk:mm");
	
	private enum FollowType
	{
		None,
		Response,
		Retweet,
		Search
	}
	
	private enum SentimentEnum
	{
		Neutral,
		Positive,
		Negative
	}
	
	public static void main(String[] args)
	{
		FollowType followType = FollowType.Response;
		SentimentEnum sentimentEnum = SentimentEnum.Negative;
		int numberOfPeople = 5;
		int startDelayInMin = random.nextInt(maxDelay);
		String screenName = "realDonaldTrump"; 
		
		// Sleep check
		Calendar now = Calendar.getInstance();
		int hour = Integer.parseInt(hourSDF.format(now.getTime()));
		String timeStamp = sdf.format(now.getTime());
		if((hour >= sleepStop) && (hour < sleepStart))
		{
			System.out.println("Running: " + timeStamp);
			TwitterAccounts twitterAccount = TwitterAccounts.ELIZA;
			Twitter twitter = TwitterUtilities.getTwitterSingleton(twitterAccount);
			// updates status of account's friends
			System.out.println("Updating friendship statuses");
			HashMap<Long,TwitterNetworkDAO> hashIds = updateTwitterNetDB(twitter);
			
			// do delay
			try {
				System.out.println("Delay " + startDelayInMin + " mins");
				Thread.sleep(startDelayInMin * 60 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			int minLeft = interval - startDelayInMin;
			int delayBtwFollows = minLeft / numberOfPeople;
			
			// get friending type
			switch(followType)
			{
			case Response:
				getFollowResponders(screenName, numberOfPeople, delayBtwFollows, 
						twitterAccount, sentimentEnum, hashIds);
				break;
			
			default:
				break;
				
			}
		}
		else
		{
			System.out.println(timeStamp + " Sleeping");
		}
				
		
		
	}

	/**
	 * 
	 * @param screenName
	 * @param numberOfPeople
	 * @param delayBtwFollows
	 * @param twitterAccount 
	 * @throws InterruptedException 
	 */
	private static void getFollowResponders(String screenName, int numberOfPeople, int delayBtwFollows,
			TwitterAccounts twitterAccount, SentimentEnum sentimentEnum, 
			HashMap<Long,TwitterNetworkDAO> hashIds) {
		int numTweets = 1;
		String sentimentMatch = "";
		ArrayList<Long> followList = new ArrayList<Long>();
		if(sentimentEnum == SentimentEnum.Negative)
		{
			sentimentMatch = "negative";
		}
		else if(sentimentEnum == SentimentEnum.Positive)
		{
			sentimentMatch = "positive";
		}
		else
		{
			System.out.println("No sentiment match");
			return;
		}
		
		ResponseList<Status> tweets = TwitterUtilities.getFromUser(screenName, numTweets, twitterAccount);
		for(Status tweet : tweets)
		{
			System.out.println("@"+screenName+": " + tweet.getText());
			ArrayList<Status> replies = getDiscussion(tweet, twitterAccount);
			for(Status reply : replies)
			{
				if((hashIds.get(reply.getUser().getId()) == null) || (!hashIds.get(reply.getUser().getId()).isFriend()))
				{
					String replyScreenName = reply.getUser().getScreenName();
					String tweetStatus = reply.getText();
					// get sentiment
					SentimentString sentiment = SentimentAnalysis.getSentiment(tweetStatus);
					System.out.println(replyScreenName + "("+reply.getUser().getId()+"): " + tweetStatus + " sentiment: " + sentiment.getSentiment());
					if(sentiment.getSentiment().equals(sentimentMatch))
					{
						followList.add(reply.getUser().getId());
						if(followList.size() >= numberOfPeople)
						{
							break;
						}
					}
				}
				
				
			}
		}
		Twitter twitter = TwitterUtilities.getTwitterSingleton(twitterAccount);
		addFollowers(twitter, followList, delayBtwFollows);
		
		
	}

	private static void addFollowers(Twitter twitter, ArrayList<Long> followList, 
			int maxDelayBtwFollows){
		for(Long followId : followList)
		{
			int delayBtwFollows = random.nextInt(maxDelayBtwFollows);
			try {
				Thread.sleep(delayBtwFollows * 60 * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			boolean allowBots = false;
			TwitterUtilities.addFollower(twitter, followId, allowBots);
		}
		
	}

	/**
	 * 
	 * @param status
	 * @param twitterAccount
	 * @return
	 */
	private static ArrayList<Status> getDiscussion(Status status, TwitterAccounts twitterAccount) {
		ArrayList<Status> replies = new ArrayList<>();
		ArrayList<Status> all = null;
		Twitter twitter = TwitterUtilities.getTwitterSingleton(twitterAccount);
	    try {
	        long id = status.getId();
	        String screenname = status.getUser().getScreenName();

	        Query query = new Query("@" + screenname + " since_id:" + id);

	        System.out.println("query string: " + query.getQuery());

	        try {
	            query.setCount(100);
	        } catch (Throwable e) {
	            // enlarge buffer error?
	            query.setCount(30);
	        }

	        QueryResult result = twitter.search(query);
	        System.out.println("result: " + result.getTweets().size());

	        all = new ArrayList<Status>();

	        do {

	            List<Status> tweets = result.getTweets();

	            for (Status tweet : tweets)
	                if (tweet.getInReplyToStatusId() == id)
	                    all.add(tweet);

	            if (all.size() > 0) {
	                for (int i = all.size() - 1; i >= 0; i--)
	                    replies.add(all.get(i));
	                all.clear();
	            }

	            query = result.nextQuery();

	            if (query != null)
	                result = twitter.search(query);

	        } while (query != null);

	    } catch (Exception e) {
	        e.printStackTrace();
	    } catch (OutOfMemoryError e) {
	        e.printStackTrace();
	    }
	    return replies;
	}

	private static HashMap<Long,TwitterNetworkDAO> updateTwitterNetDB(Twitter twitter) {
		IDs friendIds = null;
		IDs followerIds = null;
		long startId = 0;
		try {

			startId = twitter.getId();
			friendIds = friendsIDsCrawler(twitter.getId(), twitter);
			followerIds = followersIDsCrawler(startId,twitter);
		} catch (IllegalStateException | TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Get DB info
		HashMap<Long,TwitterNetworkDAO> idsHash = DatabaseAPI.getFriendHashAll();
		HashMap<Long,TwitterNetworkDAO> followerBurnDownHash = DatabaseAPI.getFollowerHash();
		
		// Check list of friends
		if(followerIds != null)
		{
			
			for(long followerId : followerIds.getIDs())
			{
				if(idsHash.get(followerId) == null)
				{
					TwitterNetworkDAO twitterNet = new TwitterNetworkDAO();
					twitterNet.setTwitterID(followerId);
					twitterNet.setFollower(true);
					twitterNet.setFollowerDate(new Date());
					
					double botScore;
					try {
						botScore = TwitterUtilities.botOrNot(followerId,twitter);
					} catch (IllegalStateException e) {
						e.printStackTrace();
						botScore = 1.0;
					} catch (TwitterException e) {
						e.printStackTrace();
						botScore = 1.0;
					}
					boolean isBot = botScore >= TwitterUtilities.botScoreThreshold;
					if(isBot)
					{
						twitterNet.setBot(true);
						twitterNet.setBotDate(new Date());
					}
					try {
						DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
					} catch (IllegalStateException | TwitterException e) {
						e.printStackTrace();
					}
					idsHash.put(followerId,twitterNet);
				}
				else if(!idsHash.get(followerId).isFollower())
				{
					TwitterNetworkDAO twitterNet = idsHash.get(followerId);
					twitterNet.setFollower(true);
					twitterNet.setFollowerDate(new Date());
										
					try {
						DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
					} catch (IllegalStateException | TwitterException e) {
						e.printStackTrace();
					}
					idsHash.put(followerId,twitterNet);
				}
				else
				{
					followerBurnDownHash.remove(followerId);
				}
			}
		}
		// Update people that are no longer following
		Set<Long> tempSet = followerBurnDownHash.keySet();
		ArrayList<Long> noLongerFollowing = new ArrayList<Long>(tempSet);
		for(Long id : noLongerFollowing)
		{
			TwitterNetworkDAO twitterNet = idsHash.get(id);
			twitterNet.setFollower(false);
			try {
				DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
			} catch (IllegalStateException | TwitterException e) {
				e.printStackTrace();
			}
			idsHash.put(id,twitterNet);
			
		}
		
		HashMap<Long,TwitterNetworkDAO> friendBurnDownHash = DatabaseAPI.getFriendHash();
		
		// Check list of friends
		if(friendIds != null)
		{
			
			for(long friendId : friendIds.getIDs())
			{
				if(idsHash.get(friendId) == null)
				{
					TwitterNetworkDAO twitterNet = new TwitterNetworkDAO();
					twitterNet.setTwitterID(friendId);
					twitterNet.setFriend(true);
					twitterNet.setFriendDate(new Date());
					idsHash.put(friendId,twitterNet);
					double botScore;
					try {
						botScore = TwitterUtilities.botOrNot(friendId,twitter);
					} catch (IllegalStateException e) {
						e.printStackTrace();
						botScore = 1.0;
					} catch (TwitterException e) {
						e.printStackTrace();
						botScore = 1.0;
					}
					boolean isBot = botScore >= TwitterUtilities.botScoreThreshold;
					if(isBot)
					{
						twitterNet.setBot(true);
						twitterNet.setBotDate(new Date());
					}
					try {
						DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
					} catch (IllegalStateException | TwitterException e) {
						e.printStackTrace();
					}
					idsHash.put(friendId,twitterNet);
				}
				else if(!idsHash.get(friendId).isFriend())
				{
					TwitterNetworkDAO twitterNet = idsHash.get(friendId);
					twitterNet.setFriend(true);
					twitterNet.setFriendDate(new Date());
					try {
						DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
					} catch (IllegalStateException | TwitterException e) {
						e.printStackTrace();
					}
					idsHash.put(friendId,twitterNet);
				}
				else
				{
					friendBurnDownHash.remove(friendId);
				}
			}
		}
		
		// Update people that are no longer following
		Set<Long> tempSet2 = friendBurnDownHash.keySet();
		ArrayList<Long> noLongerFriends = new ArrayList<Long>(tempSet2);
		for(Long id : noLongerFriends)
		{
			TwitterNetworkDAO twitterNet = idsHash.get(id);
			twitterNet.setFriend(false);
			try {
				DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
			} catch (IllegalStateException | TwitterException e) {
				e.printStackTrace();
			}
			idsHash.put(id,twitterNet);
			
		}
		
		return idsHash;
	} // 
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws TwitterException 
	 */
	public static IDs friendsIDsCrawler(long id, Twitter twitter) throws TwitterException {
		IDs ids;
		long cursor = -1;

		do{ 
		    ids = twitter.getFriendsIDs(id, cursor);
		    
		}
		while ((cursor = ids.getNextCursor()) != 0);
		return ids;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws TwitterException 
	 */
	public static IDs followersIDsCrawler(long id, Twitter twitter) throws TwitterException {
		IDs ids;
		long cursor = -1;

		do{ 
		    ids = twitter.getFollowersIDs(id, cursor);
		    
		}
		while ((cursor = ids.getNextCursor()) != 0);
		return ids;
	}
	

		
	
}
