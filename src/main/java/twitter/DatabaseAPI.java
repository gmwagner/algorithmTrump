package twitter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import utilities.DBUtilities;

public class DatabaseAPI {
	/**
	 * 
	 * @param twitterNet
	 * @return
	 */
	public static int updateInfo(TwitterNetworkDAO twitterNet) {
//		id integer NOT NULL DEFAULT nextval('"twitterConn_id_seq"'::regclass),
//		  "twitterID" bigint,d
//		  "isFollower" boolean DEFAULT false,
//		  "isFriend" boolean DEFAULT false,
//		  "isBot" boolean DEFAULT false,
//		  "followerDate" timestamp without time zone DEFAULT now(),
//		  "friendDate" timestamp without time zone DEFAULT now(),
//		  "botDate" timestamp without time zone DEFAULT now(),
//		  "createdAt" timestamp without time zone DEFAULT now(),
//		  "UpdatedAt" timestamp without time zone DEFAULT now(),
		
		int rowsInserted = 0;
		String updateString = "";
		String insertString = "";
		updateString = "UPDATE public.\"twitterConn\" SET \"updatedAt\" = now(),"
				+ "\"isFollower\" = ?, \"isFriend\" = ?, \"isBot\" = ?, "
				+ "\"followerDate\" = ?, \"friendDate\" = ?, \"botDate\" = ? "
				+ "WHERE \"twitterID\" = ?";
		insertString = "INSERT INTO public.\"twitterConn\" (\"twitterID\", \"isFollower\", "
				+ "\"isFriend\", \"isBot\", \"followerDate\", \"friendDate\", "
				+ "\"botDate\") "
				+ "SELECT ?, ?, ?, ?, ?, ?, ? "
				+ "WHERE NOT EXISTS ("
				+ "SELECT \"twitterID\" FROM public.\"twitterConn\" "
				+ "WHERE \"twitterID\" = ?)";
		
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pstUp = null;
		try
		{
			con = DBUtilities.connectDB();
			pstUp = con.prepareStatement(updateString);
			pstUp.setBoolean(1, twitterNet.isFollower()); 
			pstUp.setBoolean(2, twitterNet.isFriend());
			pstUp.setBoolean(3, twitterNet.isBot());
			pstUp.setTimestamp(4, new java.sql.Timestamp(twitterNet.getFollowerDate().getTime())); 
			pstUp.setTimestamp(5, new java.sql.Timestamp(twitterNet.getFriendDate().getTime()));
			pstUp.setTimestamp(6, new java.sql.Timestamp(twitterNet.getBotDate().getTime()));
			pstUp.setLong(7, twitterNet.getTwitterID());
			
			pstUp.executeUpdate();
			
			pst = con.prepareStatement(insertString);
			pst.setLong(1, twitterNet.getTwitterID());
			pst.setBoolean(2, twitterNet.isFollower()); 
			pst.setBoolean(3, twitterNet.isFriend());
			pst.setBoolean(4, twitterNet.isBot());
			pst.setTimestamp(5, new java.sql.Timestamp(twitterNet.getFollowerDate().getTime())); 
			pst.setTimestamp(6, new java.sql.Timestamp(twitterNet.getFriendDate().getTime()));
			pst.setTimestamp(7, new java.sql.Timestamp(twitterNet.getBotDate().getTime()));
			pst.setLong(8, twitterNet.getTwitterID());
			rowsInserted = pst.executeUpdate();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        		pstUp.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
      	}
  	    catch (SQLException ex) {
  	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
  	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
  		}
		}
		
		return rowsInserted;
	}
	
	public static int updateInfo(String screenName, TwitterNetworkDAO twitterNet) {
//		id integer NOT NULL DEFAULT nextval('"twitterConn_id_seq"'::regclass),
//		  "twitterID" bigint,d
//		  "isFollower" boolean DEFAULT false,
//		  "isFriend" boolean DEFAULT false,
//		  "isBot" boolean DEFAULT false,
//		  "followerDate" timestamp without time zone DEFAULT now(),
//		  "friendDate" timestamp without time zone DEFAULT now(),
//		  "botDate" timestamp without time zone DEFAULT now(),
//		  "createdAt" timestamp without time zone DEFAULT now(),
//		  "UpdatedAt" timestamp without time zone DEFAULT now(),
		
		int rowsInserted = 0;
		String updateString = "";
		String insertString = "";
		updateString = "UPDATE public.\"twitterConn\" SET \"screenName\" = ?, \"updatedAt\" = now(),"
				+ "\"isFollower\" = ?, \"isFriend\" = ?, \"isBot\" = ?, "
				+ "\"followerDate\" = ?, \"friendDate\" = ?, \"botDate\" = ? "
				+ "WHERE \"twitterID\" = ?";
		insertString = "INSERT INTO public.\"twitterConn\" (\"twitterID\", \"screenName\", \"isFollower\", "
				+ "\"isFriend\", \"isBot\", \"followerDate\", \"friendDate\", "
				+ "\"botDate\") "
				+ "SELECT ?, ?, ?, ?, ?, ?, ?, ? "
				+ "WHERE NOT EXISTS ("
				+ "SELECT \"twitterID\" FROM public.\"twitterConn\" "
				+ "WHERE \"twitterID\" = ?)";
		
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pstUp = null;
		try
		{
			con = DBUtilities.connectDB();
			pstUp = con.prepareStatement(updateString);
			pstUp.setString(1, screenName); 
			pstUp.setBoolean(2, twitterNet.isFollower()); 
			pstUp.setBoolean(3, twitterNet.isFriend());
			pstUp.setBoolean(4, twitterNet.isBot());
			pstUp.setTimestamp(5, new java.sql.Timestamp(twitterNet.getFollowerDate().getTime())); 
			pstUp.setTimestamp(6, new java.sql.Timestamp(twitterNet.getFriendDate().getTime()));
			pstUp.setTimestamp(7, new java.sql.Timestamp(twitterNet.getBotDate().getTime()));
			pstUp.setLong(8, twitterNet.getTwitterID());
			
			pstUp.executeUpdate();
			
			pst = con.prepareStatement(insertString);
			pst.setLong(1, twitterNet.getTwitterID());
			pst.setString(2, screenName); 
			pst.setBoolean(3, twitterNet.isFollower()); 
			pst.setBoolean(4, twitterNet.isFriend());
			pst.setBoolean(5, twitterNet.isBot());
			pst.setTimestamp(6, new java.sql.Timestamp(twitterNet.getFollowerDate().getTime())); 
			pst.setTimestamp(7, new java.sql.Timestamp(twitterNet.getFriendDate().getTime()));
			pst.setTimestamp(8, new java.sql.Timestamp(twitterNet.getBotDate().getTime()));
			pst.setLong(9, twitterNet.getTwitterID());
			rowsInserted = pst.executeUpdate();
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        		pstUp.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
      	}
  	    catch (SQLException ex) {
  	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
  	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
  		}
		}
		
		return rowsInserted;
	}
	
	/**
	 * 
	 * @param twitterID
	 * @return
	 */
	public static TwitterNetworkDAO getFromId(long twitterID)
	{
		TwitterNetworkDAO twitterNet = new TwitterNetworkDAO();
		
		String selectString = "SELECT \n" + 
				"  \"twitterConn\".\"twitterID\", \n" + 
				"  \"twitterConn\".\"isFollower\", \n" + 
				"  \"twitterConn\".\"isFriend\", \n" + 
				"  \"twitterConn\".\"isBot\", \n" + 
				"  \"twitterConn\".\"followerDate\", \n" + 
				"  \"twitterConn\".\"friendDate\", \n" + 
				"  \"twitterConn\".\"botDate\", \n" + 
				"  \"twitterConn\".\"createdAt\", \n" + 
				"  \"twitterConn\".\"updatedAt\"\n" + 
				"FROM \n" + 
				"  public.\"twitterConn\"\n" + 
				"WHERE \n" + 
				"  \"twitterConn\".\"twitterID\" = ?"; 
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			con = DBUtilities.connectDB();
			pst = con.prepareStatement(selectString);
			pst.setLong(1, twitterID);
			rs = pst.executeQuery();
			while(rs.next())
			{

				twitterNet = new TwitterNetworkDAO(
					rs.getLong("twitterID"),rs.getBoolean("isFollower"), rs.getBoolean("isFriend"),
					rs.getBoolean("isBot"),new Date(rs.getTimestamp("followerDate").getTime()), 
					new Date(rs.getTimestamp("friendDate").getTime()), new Date(rs.getTimestamp("botDate").getTime()), 
					new Date(rs.getTimestamp("createdAt").getTime()), new Date(rs.getTimestamp("updatedAt").getTime()));
				
			}
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}

	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return twitterNet;
	}
	
	/**
	 * 
	 * @param twitterID
	 * @return
	 */
	public static HashMap<Long, TwitterNetworkDAO> getFriendHashAll()
	{
		HashMap<Long, TwitterNetworkDAO> idHash = new HashMap<Long, TwitterNetworkDAO>();
		
		String selectString = "SELECT \n" + 
				"  \"twitterConn\".\"twitterID\", \n" + 
				"  \"twitterConn\".\"isFollower\", \n" + 
				"  \"twitterConn\".\"isFriend\", \n" + 
				"  \"twitterConn\".\"isBot\", \n" + 
				"  \"twitterConn\".\"followerDate\", \n" + 
				"  \"twitterConn\".\"friendDate\", \n" + 
				"  \"twitterConn\".\"botDate\", \n" + 
				"  \"twitterConn\".\"createdAt\", \n" + 
				"  \"twitterConn\".\"updatedAt\"\n" + 
				"FROM \n" + 
				"  public.\"twitterConn\"";
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			con = DBUtilities.connectDB();
			pst = con.prepareStatement(selectString);
			rs = pst.executeQuery();
			while(rs.next())
			{
				TwitterNetworkDAO twitterNet = new TwitterNetworkDAO(
						rs.getLong("twitterID"),rs.getBoolean("isFollower"), rs.getBoolean("isFriend"),
						rs.getBoolean("isBot"),new Date(rs.getTimestamp("followerDate").getTime()), 
						new Date(rs.getTimestamp("friendDate").getTime()), new Date(rs.getTimestamp("botDate").getTime()), 
						new Date(rs.getTimestamp("createdAt").getTime()), new Date(rs.getTimestamp("updatedAt").getTime()));
				idHash.put(rs.getLong("twitterID"), twitterNet);
				
			}
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}

	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return idHash;
	}

	/**
	 * 
	 * @param twitterID
	 * @return
	 */
	public static HashMap<Long, TwitterNetworkDAO> getFollowerHash()
	{
		HashMap<Long, TwitterNetworkDAO> idHash = new HashMap<Long, TwitterNetworkDAO>();
		
		String selectString = "SELECT \n" + 
				"  \"twitterConn\".\"twitterID\", \n" + 
				"  \"twitterConn\".\"isFollower\", \n" + 
				"  \"twitterConn\".\"isFriend\", \n" + 
				"  \"twitterConn\".\"isBot\", \n" + 
				"  \"twitterConn\".\"followerDate\", \n" + 
				"  \"twitterConn\".\"friendDate\", \n" + 
				"  \"twitterConn\".\"botDate\", \n" + 
				"  \"twitterConn\".\"createdAt\", \n" + 
				"  \"twitterConn\".\"updatedAt\"\n" + 
				"FROM \n" + 
				"  public.\"twitterConn\"" +
				"WHERE \n" + 
				"  \"twitterConn\".\"isFollower\" = ?"; 
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			con = DBUtilities.connectDB();
			pst = con.prepareStatement(selectString);
			pst.setBoolean(1, true);
			rs = pst.executeQuery();
			while(rs.next())
			{
				TwitterNetworkDAO twitterNet = new TwitterNetworkDAO(
						rs.getLong("twitterID"),rs.getBoolean("isFollower"), rs.getBoolean("isFriend"),
						rs.getBoolean("isBot"),new Date(rs.getTimestamp("followerDate").getTime()), 
						new Date(rs.getTimestamp("friendDate").getTime()), new Date(rs.getTimestamp("botDate").getTime()), 
						new Date(rs.getTimestamp("createdAt").getTime()), new Date(rs.getTimestamp("updatedAt").getTime()));
				idHash.put(rs.getLong("twitterID"), twitterNet);
				
			}
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}

	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return idHash;
	}

	/**
	 * 
	 * @param twitterID
	 * @return
	 */
	public static HashMap<Long, TwitterNetworkDAO> getFriendHash()
	{
		HashMap<Long, TwitterNetworkDAO> idHash = new HashMap<Long, TwitterNetworkDAO>();
		
		String selectString = "SELECT \n" + 
				"  \"twitterConn\".\"twitterID\", \n" + 
				"  \"twitterConn\".\"isFollower\", \n" + 
				"  \"twitterConn\".\"isFriend\", \n" + 
				"  \"twitterConn\".\"isBot\", \n" + 
				"  \"twitterConn\".\"followerDate\", \n" + 
				"  \"twitterConn\".\"friendDate\", \n" + 
				"  \"twitterConn\".\"botDate\", \n" + 
				"  \"twitterConn\".\"createdAt\", \n" + 
				"  \"twitterConn\".\"updatedAt\"\n" + 
				"FROM \n" + 
				"  public.\"twitterConn\"" +
				"WHERE \n" + 
				"  \"twitterConn\".\"isFriend\" = ?"; 
		
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try
		{
			con = DBUtilities.connectDB();
			pst = con.prepareStatement(selectString);
			pst.setBoolean(1, true);
			rs = pst.executeQuery();
			while(rs.next())
			{
				TwitterNetworkDAO twitterNet = new TwitterNetworkDAO(
						rs.getLong("twitterID"),rs.getBoolean("isFollower"), rs.getBoolean("isFriend"),
						rs.getBoolean("isBot"),new Date(rs.getTimestamp("followerDate").getTime()), 
						new Date(rs.getTimestamp("friendDate").getTime()), new Date(rs.getTimestamp("botDate").getTime()), 
						new Date(rs.getTimestamp("createdAt").getTime()), new Date(rs.getTimestamp("updatedAt").getTime()));
				idHash.put(rs.getLong("twitterID"), twitterNet);
				
			}
			
		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		} 
		finally
		{
			try{
	        	if(pst != null)
	        	{
	        		pst.close();
	        	}

	        	if(rs != null)
	        	{
	        		rs.close();
	        	}
	        	if(con != null)
	        	{
	        		DBUtilities.closeDB(con);
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(DatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
		
		return idHash;
	}

}

