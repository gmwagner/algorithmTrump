package twitter;

import java.util.Date;

public class TwitterNetworkDAO {
//	id integer NOT NULL DEFAULT nextval('"twitterConn_id_seq"'::regclass),
//	  "twitterID" bigint,
//	  "isFollower" boolean DEFAULT false,
//	  "isFriend" boolean DEFAULT false,
//	  "isBot" boolean DEFAULT false,
//	  "followerDate" timestamp without time zone DEFAULT now(),
//	  "friendDate" timestamp without time zone DEFAULT now(),
//	  "botDate" timestamp without time zone DEFAULT now(),
//	  "createdAt" timestamp without time zone DEFAULT now(),
//	  "UpdatedAt" timestamp without time zone DEFAULT now(),
	
	private long twitterID     = 0;
	private String associatedScreenName = "";
	private boolean isFollower = false;
	private boolean isFriend   = false;
	private boolean isBot      = false;
	private boolean followBack = false;
	private Date followerDate  = new Date();
	private Date friendDate    = new Date();
	private Date botDate       = new Date();
	private Date createdAt     = new Date();
	private Date updatedAt     = new Date();
	private Date followBackAt  = new Date();
	
	// constructors 
	public TwitterNetworkDAO()
	{
		
	}
	
	public TwitterNetworkDAO(long twitterID, boolean isFollower, boolean isFriend, boolean isBot, Date followerDate,
			Date friendDate, Date botDate, Date createdAt, Date updatedAt) {
		super();
		this.twitterID = twitterID;
		this.isFollower = isFollower;
		this.isFriend = isFriend;
		this.isBot = isBot;
		this.followerDate = followerDate;
		this.friendDate = friendDate;
		this.botDate = botDate;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}
	
	public TwitterNetworkDAO(long twitterID, String associatedScreenName, boolean isFollower, boolean isFriend, boolean isBot, Date followerDate,
			Date friendDate, Date botDate, Date createdAt, Date updatedAt) {
		super();
		this.twitterID = twitterID;
		this.associatedScreenName = associatedScreenName;
		this.isFollower = isFollower;
		this.isFriend = isFriend;
		this.isBot = isBot;
		this.followerDate = followerDate;
		this.friendDate = friendDate;
		this.botDate = botDate;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public TwitterNetworkDAO(long twitterID, String associatedScreenName, boolean isFollower, boolean isFriend,
			boolean isBot, boolean followBack, Date followerDate, Date friendDate, Date botDate, Date createdAt,
			Date updatedAt, Date followBackAt) {
		super();
		this.twitterID = twitterID;
		this.associatedScreenName = associatedScreenName;
		this.isFollower = isFollower;
		this.isFriend = isFriend;
		this.isBot = isBot;
		this.followBack = followBack;
		this.followerDate = followerDate;
		this.friendDate = friendDate;
		this.botDate = botDate;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.followBackAt = followBackAt;
	}

	public long getTwitterID() {
		return twitterID;
	}

	public void setTwitterID(long twitterID) {
		this.twitterID = twitterID;
	}
	
	public String getAssociatedScreenName() {
		return associatedScreenName;
	}

	public void setAssociatedScreenName(String associatedScreenName) {
		this.associatedScreenName = associatedScreenName;
	}

	public boolean isFollower() {
		return isFollower;
	}

	public void setFollower(boolean isFollower) {
		this.isFollower = isFollower;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	public boolean isBot() {
		return isBot;
	}

	public void setBot(boolean isBot) {
		this.isBot = isBot;
	}

	public Date getFollowerDate() {
		return followerDate;
	}

	public void setFollowerDate(Date followerDate) {
		this.followerDate = followerDate;
	}

	public Date getFriendDate() {
		return friendDate;
	}

	public void setFriendDate(Date friendDate) {
		this.friendDate = friendDate;
	}

	public Date getBotDate() {
		return botDate;
	}

	public void setBotDate(Date botDate) {
		this.botDate = botDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isFollowBack() {
		return followBack;
	}

	public void setFollowBack(boolean followBack) {
		this.followBack = followBack;
	}

	public Date getFollowBackAt() {
		return followBackAt;
	}

	public void setFollowBackAt(Date followBackAt) {
		this.followBackAt = followBackAt;
	}
	
	
	
}

