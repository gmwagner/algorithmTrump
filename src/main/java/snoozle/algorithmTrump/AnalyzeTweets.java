package snoozle.algorithmTrump;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import twitter4j.ResponseList;
import twitter4j.Status;
import utilities.NLPTools;
import utilities.TwitterUtilities;

public class AnalyzeTweets {
	
	private static String twitterHandle = "realDonaldTrump"; 
	private static final TwitterUtilities.TwitterAccounts twitterAccount = TwitterUtilities.TwitterAccounts.ALGORITHM_TRUMP;
	private static final int numTweets = 20;
	
	private static TokenNameFinderModel modelName = null;
	private static TokenizerModel modelToken = null;
	private static POSModel modelPOS = null;
	
	/***
	 * 
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException
	{
		loadTokenModel();
		loadNameModel();
		loadPOSModel();
		SentenceModel modelSent = NLPTools.getSentenceModel();
		SentenceDetectorME sentenceDetector = new SentenceDetectorME(modelSent);
		NameFinderME nameFinder = new NameFinderME(modelName);
		Tokenizer tokenizer = new TokenizerME(modelToken);
		POSTaggerME tagger = new POSTaggerME(modelPOS);
		ResponseList<Status> responses = TwitterUtilities.getFromUser(twitterHandle, numTweets, twitterAccount);
		for(Status status : responses)
		{
			String sentences[] = sentenceDetector.sentDetect(status.getText());
			for(String sentence : sentences)
			{
				System.out.println(sentence);
				String tokens[] = tokenizer.tokenize(sentence);
				Span nameSpans[] = nameFinder.find(tokens);
				String tags[] = tagger.tag(tokens);
				for(Span nameSpan : nameSpans)
				{
					System.out.print("Name 1: ");
					for(int ii = nameSpan.getStart(); ii < nameSpan.getEnd(); ii++)
					{
						System.out.print(tokens[ii] + " ");
					}
					System.out.println();
				}
			}
		}
	}
	
	
	
	/***
	 * 
	 * @throws FileNotFoundException
	 */
	private static void loadNameModel() throws FileNotFoundException
	{
		InputStream modelIn = new FileInputStream(utilities.NLPTools.OPEN_NLP_MODEL_DIR+"en-ner-person.bin");

		try {
		   modelName = new TokenNameFinderModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
	}
	
	/***
	 * 
	 * @throws FileNotFoundException
	 */
	private static void loadTokenModel() throws FileNotFoundException
	{
		InputStream modelIn = new FileInputStream(utilities.NLPTools.OPEN_NLP_MODEL_DIR+"en-token.bin");

		try {
		  modelToken = new TokenizerModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
	}
	
	private static void loadPOSModel()
	{
		InputStream modelIn = null;

		try {
		  modelIn = new FileInputStream(utilities.NLPTools.OPEN_NLP_MODEL_DIR+"en-pos-maxent.bin");
		  modelPOS = new POSModel(modelIn);
		}
		catch (IOException e) {
		  // Model loading failed, handle the error
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
	}
}
