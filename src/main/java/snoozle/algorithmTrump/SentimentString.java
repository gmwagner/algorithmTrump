package snoozle.algorithmTrump;

import java.util.Date;

public class SentimentString {
	// attributes
	private String string = "";
	private String sentiment = "";
	private String source = "";
	private String link = "";
	private Date generationDate;
	private double probablity = 0.0;
	private double classAccuracy = 0.0;
	
	
	// constructors
	public SentimentString(String string, String sentiment, Date generationDate, double probablity,
			double classAccuracy) {
		super();
		this.string = string;
		this.sentiment = sentiment;
		this.generationDate = generationDate;
		this.probablity = probablity;
		this.classAccuracy = classAccuracy;
	}


	public SentimentString() {
		super();
	}


	public SentimentString(String string, Date generationDate) {
		super();
		this.string = string;
		this.generationDate = generationDate;
	}
	

	public SentimentString(String string, Date generationDate, String source) {
		super();
		this.string = string;
		this.generationDate = generationDate;
		this.source = source;
	}
	
	public SentimentString(String string, Date generationDate, String source, String link) {
		super();
		this.string = string;
		this.generationDate = generationDate;
		this.source = source;
		this.setLink(link);
	}
	
	public SentimentString(String string) {
		super();
		this.string = string;
		this.generationDate = new Date();
	}

	// Gettes and setters
	public String getString() {
		return string;
	}


	public void setString(String string) {
		this.string = string;
	}


	public String getSentiment() {
		return sentiment;
	}


	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}


	public Date getGenerationDate() {
		return generationDate;
	}


	public void setGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
	}


	public double getProbablity() {
		return probablity;
	}


	public void setProbablity(double probablity) {
		this.probablity = probablity;
	}


	public double getClassAccuracy() {
		return classAccuracy;
	}


	public void setClassAccuracy(double classAccuracy) {
		this.classAccuracy = classAccuracy;
	}

	public String getSource() {
		return source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	@Override
	public String toString() {
		return "StringSentiment [string=" + string + ", sentiment=" + sentiment + ", generationDate=" + generationDate
				+ ", probablity=" + probablity + ", classAccuracy=" + classAccuracy + "]";
	}


	public String getLink() {
		return link;
	}


	public void setLink(String link) {
		this.link = link;
	}
	
	
	
}
