package snoozle.algorithmTrump;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import machineLearning.SentimentAnalysis;
import opennlp.tools.parser.ParserFactory;
import opennlp.tools.parser.ParserModel;
import opennlp.tools.cmdline.parser.ParserTool;
import opennlp.tools.parser.Parse;
import opennlp.tools.parser.Parser;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import rss.Feed;
import rss.FeedDB;
import rss.FeedMessage;
import rss.RSSFeedParser;
import utilities.NLPTools;
import utilities.TwitterUtilities;

/**
 * Hello world!
 *
 */
public class DailyRunner 
{
	private final static SimpleDateFormat sdfPubDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
	private final static double sentimentThres = 0.90;
	private final static int randSeed = 0;
	protected static Random random = new Random();

	private final static int negSize = 3;
	private final static int posSize = 3;
	
	
	private enum PosWords
	{
		TREMENDOUS("TREMENDOUS!"),
		NICE("NICE!"),
		WATCH("WATCH!");
		
		private final String string;
		
		PosWords(String string)
		{
			this.string = string;
		}
		
		public String getWord()
		{
			return this.string;
		}
		
	}
	
	private enum NegWords
	{
		DISHONEST("DISHONEST!"),
		SAD("SAD!"),
		TOTAL_DISASTER("TOTAL DISASTER!");
		
		private final String string;
		
		NegWords(String string)
		{
			this.string = string;
		}
		
		public String getWord()
		{
			return this.string;
		}
	}
	
	/***
	 * 
	 * @param args
	 * @throws ParseException
	 * @throws FileNotFoundException 
	 */
    public static void main( String[] args ) throws ParseException, FileNotFoundException
    {
    	String searchTerm = "Trump";

		
    	random.setSeed(randSeed);
	    //Connection con = DBUtilities.connectDB();
	    ArrayList<FeedDB> feeds = new ArrayList<FeedDB>();
	    //FeedDatabaseAPI.retrieveAll(con, feeds);
	    feeds.add(new FeedDB("http://rss.cnn.com/rss/cnn_allpolitics.rss"));
	    //feeds.add(new FeedDB("http://rss.nytimes.com/services/xml/rss/nyt/Politics.xml"));
	    feeds.add(new FeedDB("http://feeds.foxnews.com/foxnews/latest"));
	    ArrayList<SentimentString> sentiments = new ArrayList<SentimentString>();
	    ParserModel parseModel = NLPTools.getParseModel();
	    Parser parserOLP = ParserFactory.create(parseModel);
	    SentenceModel sentenceModel = NLPTools.getSentenceModel();
	    SentenceDetectorME sentenceDetector = new SentenceDetectorME(sentenceModel);
	    for (FeedDB feedDB : feeds)
	    {
	    	RSSFeedParser parser = new RSSFeedParser(feedDB.getFeed());
	    	int category = feedDB.getCategory();
	    	
	    	Feed feed = parser.readFeed();
	    	String[] titleSplit = feed.getDescription().split(" ");
	    	String source = "";
	    	if(titleSplit.length > 1)
	    	{
	    		source = titleSplit[0];
	    	}
	    	else
	    	{
	    		titleSplit = feed.getTitle().split(" ");
	    		if(titleSplit.length == 1)
	    		{
	    			source = titleSplit[0];
	    		}
	    	}
	    	for (FeedMessage message : feed.getMessages()) {
	    		String pubDate = feed.getPubDate();
	    		if(pubDate.equals("") )
	    		{
	    			pubDate = sdfPubDate.format(new Date());
	    		}
	    		if(message.getDescription().contains(searchTerm))
	    		{
			    	SentimentString sentimentString = new SentimentString(message.getDescription(),
			    			sdfPubDate.parse(pubDate),source, message.getLink());
			    	sentiments.add(sentimentString);
	    		}
	    		else if(message.getTitle().contains(searchTerm))
	    		{
	    			SentimentString sentimentString = new SentimentString(message.getTitle(),
			    			sdfPubDate.parse(pubDate),source, message.getLink());
			    	sentiments.add(sentimentString);
	    		}
		    	//NewsDatabaseAPI.addStory(con, newsInfo);
		    }
	    }
	    sentiments = SentimentAnalysis.getSentiment(sentiments);
	    for(SentimentString sentiment : sentiments)
	    {
	    	System.out.println(sentiment.getString());

			String sentences[] = sentenceDetector.sentDetect(sentiment.getString());
			Parse topParses[] = ParserTool.parseLine(sentences[0], parserOLP, 1);
			topParses[0].show();
			
	    	System.out.println(String.format("%s Probablity Correct: %s", sentiment.getSentiment(), sentiment.getProbablity()));
	    	if(sentiment.getProbablity() > sentimentThres)
	    	{
	    		String subject = getSubject(sentiment.getString());
	    		String mess = "";

		    	if(sentiment.getSentiment().equalsIgnoreCase("negative"))
		    	{
		    		int negNum = random.nextInt(negSize);
		    		NegWords negWord = NegWords.values()[negNum];
		    		if(subject.equalsIgnoreCase("Trump") || subject.equals(""))
		    		{
		    			mess = String.format("Fake News %s is failing, %s %s", sentiment.getSource(), negWord.getWord(), sentiment.getLink() );
		    		}
		    		else
		    		{
		    			mess = String.format("%s can't get facts straight %s %s", subject, negWord.getWord(), sentiment.getLink());
		    		}
	
		    	}
		    	else if(sentiment.getSentiment().equalsIgnoreCase("positive"))
		    	{
		    		int posNum = random.nextInt(posSize);
		    		PosWords posWord = PosWords.values()[posNum];
		    		if(subject.equalsIgnoreCase("Trump") || subject.equals(""))
		    		{
		    			mess = String.format("%s get it right %s %s", sentiment.getSource(), posWord.getWord(), sentiment.getLink() );
		    		}
		    		else
		    		{
		    			mess = String.format("%s is a class act %s %s", subject, posWord.getWord(), sentiment.getLink());
		    		}
		    		// remove when better
		    		continue;
		    	}
		    	else
		    	{
		    		System.out.println("Not positive or negative, weird");
		    		continue;
		    	}

    			System.out.println(mess);
	    		TwitterUtilities.tweetMessage(mess, TwitterUtilities.TwitterAccounts.ALGORITHM_TRUMP);
	    	}
	    	
	    }
	    
	    //DBUtilities.closeDB(con);
    }
    
    /***
	 * 
	 * @param string
	 * @return
	 */
	private static String getSubject(String string) 
	{
		String subject = "";
		String[] splitString = string.replaceAll("/[.,\\/#!$%\\^&\\*;:{}=\\-_`~()]/g","").split(" ");
		int properCount = -1; // number of proper string in front of sentence
		for(String token : splitString)
		{
			
			if(Character.isUpperCase(token.codePointAt(0)))
			{
				properCount++;
			}
			else
			{
				break;
			}
		}
		
		if(properCount > 0)
		{
			subject = splitString[properCount];
		}
		return subject;
	}
}
