package snoozle.algorithmTrump;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import eliza.ElizaMain;
import twitter.DatabaseAPI;
import twitter.TwitterNetworkDAO;
import twitter4j.DirectMessage;
import twitter4j.ResponseList;
import twitter4j.Status;
import utilities.TwitterUtilities;

public class TweetGetter {

	private static boolean elizaAlive;
	private static final String twitterHandle = "realDonaldTrump"; 
	
	private static final TwitterUtilities.TwitterAccounts twitterAccount = TwitterUtilities.TwitterAccounts.ELIZA;
	private static final int numTweets = 20;
	private static final int hoursBetweenRuns = 1; 
	private static final int maxLength = 140;
	private static final double percentOfConnections = 0.01;

	private static Calendar cCurrent = Calendar.getInstance();
	private static ElizaMain elizaMain = new ElizaMain();
	private static final String elizaScript = "/home/greg/workspace/algorithmTrump/src/main/resources/script";
	
	public static void main(String[] args)
	{		
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/dd/MM HH:mm:ss");
		String timeStamp = sdf.format(c.getTime());
		System.out.println("Started at " + timeStamp);
		int res = elizaMain.readScript(true, elizaScript);
		if(res != 0)
		{
			System.out.println("Eliza Fail!");
			elizaAlive = false;
		}
		else
		{
			elizaAlive = true;
		}
		
		
		// Get twitter responses from handle
		System.out.println("Running replies to "+ twitterHandle);
		replyToHandle();
		
		
		// Check for mentions
		System.out.println("Responing to mentions");
		RespondToMentions();
		
		
		// Randomly Reply to Connections
		System.out.println("Randomly replying to connections");
		//replyToFollowers();
		
		
		// Check for direct messages
		//respondToDMs();
		
		//ArrayList<SentimentString> sentiments = SentimentAnalysis.getSentiment(sentimentsRaw);
		c = Calendar.getInstance();
		timeStamp = sdf.format(c.getTime());
		System.out.println("Stopping... "+timeStamp);
	}
	
	/**
	 * 
	 */
	private static void replyToFollowers() {
		
		Random random = new Random(Calendar.getInstance().getTimeInMillis());
		HashMap<Long, TwitterNetworkDAO> followerHash = DatabaseAPI.getFollowerHash();
		ArrayList<Long> idArray = new ArrayList<Long>(followerHash.keySet());
		int hoursInDay = 24;
		
		int numOfFollowers = idArray.size();
		double countDouble = numOfFollowers * (percentOfConnections/(double)(hoursInDay)); // try to reach N percent in a day
		long followMax = 0; 
		if(countDouble > 1.0)
		{
			followMax = Math.round(countDouble);
		}
		else
		{
			Double prob = random.nextDouble();
			if(prob < countDouble)
			{
				followMax = 1;
			}
		}
		
		int todayDayNum = cCurrent.get(Calendar.DAY_OF_YEAR);
		long ii = 0, count = 0;
		int maxTry = (int) (followMax * 100);
		while( (ii < followMax) && (count < maxTry))
		{
			count++;
			int randNum = random.nextInt(numOfFollowers);
			long id = idArray.get(randNum);
			System.out.println("ID: " + id);
			ResponseList<Status> responses = TwitterUtilities.getFromUser(id, numTweets, twitterAccount);
			if(responses == null)
			{
				continue;
			}
			if(responses.size() > 0)
			{
				// Only reply when twitted in the last day and is not a reply to someone else 
				for(Status status : responses)
				{
					Calendar cStatus = Calendar.getInstance();
					cStatus.setTime(status.getCreatedAt());
					int statusDayNum = cStatus.get(Calendar.DAY_OF_YEAR);
					
					String tweetStatus = status.getText();
					
					boolean isRetweet = tweetStatus.contains("RT ");
					//boolean isResponse = tweetStatus.substring(0,1).contains("@") || tweetStatus.substring(0,1).contains(".@");
					if((statusDayNum >= (todayDayNum - 1)) && !isRetweet /*&& !isResponse*/)
					{
						tweetStatus = cleanUpStatusText(tweetStatus);
						ElizaMain elizaTemp = new ElizaMain();
						int res = elizaTemp.readScript(true, elizaScript);
						if(res != 0)
						{
							// kill if fail
							elizaAlive = false;
							return;
						}
						String processedText = elizaTemp.processInput(tweetStatus);
						// Default text might be insulting if message is in earnest
						if(!isDefaultEliza(processedText) && !isYouResponseEliza(processedText) 
						&& !processedText.equals("Why do you ask ?"))
						{
							String tempHandle = status.getUser().getScreenName();
							System.out.println(tempHandle + ": " + tweetStatus + " >>> "  + processedText);
							TwitterUtilities.sendReply(status.getId(), processedText, tempHandle, twitterAccount);

							ii++;	
							break;
						}
					}
				}// for
						
			}
		}// while
	}

	private static boolean isDefaultEliza(String processedText) {
//	    reasmb: I'm not sure I understand you fully.
//	    reasmb: Please go on.
//	    reasmb: What does that suggest to you ?
//	    reasmb: Do you feel strongly about discussing such things ?
//		reasmb: That is interesting.  Please continue.
//		reasmb: Tell me more about that.
//		reasmb: Does talking about this bother you ?
		boolean returnVal = false;
		if((processedText.equals("I'm not sure I understand you fully.")) || 
		   (processedText.equals("Please go on.")) ||
		   (processedText.equals("What does that suggest to you ?")) || 
		   (processedText.equals("Do you feel strongly about discussing such things ?")) || 
		   (processedText.equals("That is interesting.  Please continue.")) || 
		   (processedText.equals("Tell me more about that.")) || 
		   (processedText.equals("Does talking about this bother you ?")))
		{
			returnVal = true;
		}
		return returnVal;
	}
	
	/*
	 * 
	 */
	private static boolean isYouResponseEliza(String processedText) {

//	    reasmb: We were discussing you -- not me.
//	    reasmb: Oh, I (2) ?
//	    reasmb: You're not really talking about me -- are you ?
//	    reasmb: What are your feelings now ?
		boolean returnVal = false;
		if((processedText.equals("We were discussing you -- not me.")) || 
		   (processedText.equals("You're not really talking about me -- are you ?")))
		{
			returnVal = true;
		}
		return returnVal;
	}

	/**
	 * 
	 * @param tweetStatus
	 * @return
	 */
	private static String cleanUpStatusText(String tweetStatus) {
		// if too long only use first sentence
		if(tweetStatus.length() >= maxLength)
		{
			int firstSentenceEnd = maxLength;
			// find question mark point
			int questionLoc = tweetStatus.indexOf("?");
			// find explimation point 
			int bashLoc = tweetStatus.indexOf("!");
			// find period
			int periodLoc = tweetStatus.indexOf(".");
			if((questionLoc != -1) && (questionLoc < firstSentenceEnd))
			{
				firstSentenceEnd = questionLoc;
			}
			if((bashLoc != -1) && (bashLoc < firstSentenceEnd))
			{
				firstSentenceEnd = bashLoc;
			}
			if((periodLoc != -1) && (periodLoc < firstSentenceEnd))
			{
				firstSentenceEnd = periodLoc;
			}
			tweetStatus = tweetStatus.substring(0,firstSentenceEnd);
		}
		
		// remove links from tweet status
		int httpLoc = tweetStatus.indexOf("http");
		if(httpLoc != -1)
		{
			tweetStatus = tweetStatus.substring(0, httpLoc);
		}
		
		return tweetStatus;
	}

	private static void replyToHandle()
	{
		replyToHandle(twitterHandle, twitterAccount);
	}

	/***
	 * 
	 */
	private static void replyToHandle(String screenName, TwitterUtilities.TwitterAccounts twitterAcct) {
		ResponseList<Status> responses = TwitterUtilities.getFromUser(screenName, numTweets, twitterAcct);
		if(responses == null)
		{
			return;
		}
		ArrayList<SentimentString> sentimentsRaw = new ArrayList<SentimentString>();
		for(int ii = responses.size()-1; ii >= 0; ii--)
		{
			Status status = responses.get(ii);
			SentimentString temp = new SentimentString(status.getText(),status.getCreatedAt());
			sentimentsRaw.add(temp);
			Calendar cCreated = Calendar.getInstance();
			cCreated.setTime(status.getCreatedAt());
			cCreated.add(Calendar.HOUR, hoursBetweenRuns); // check if any new tweets in the last hour
			String tweetStatus = temp.getString();
			// skip if retweet
			boolean isRetweet = tweetStatus.contains("RT "); 
			
			if (elizaAlive && cCreated.after(cCurrent) && !isRetweet)
			{	
				tweetStatus = cleanUpStatusText(tweetStatus);
				
				String processedText = elizaMain.processInput(tweetStatus);
				System.out.println(tweetStatus + " >>> "  + processedText);
				
				if(!isDefaultEliza(processedText))
				{
				
					TwitterUtilities.commentRetweet(status.getId(), processedText, twitterHandle, twitterAccount);
					
				}
				else
				{
					System.out.println("Is default text. TOTAL FAILURE!!!!");
					// Since the orginal algo does them in order, I'll randomly generate them
					Random random = new Random(Calendar.getInstance().getTimeInMillis());
					int numberOfResponses = 7;
					switch(random.nextInt(numberOfResponses))
					{
					case 0:
						processedText = "I'm not sure I understand you fully.";
						break;
						
					case 1:
						processedText = "Please go on.";
						break;
						
					case 2:
						processedText = "What does that suggest to you ?";
						break;
						
					case 3:
						processedText = "Do you feel strongly about discussing such things ?";
						break;
						
					case 4:
						processedText = "That is interesting.  Please continue.";
						break;
						
					case 5:
						processedText = "Tell me more about that.";
						break;
						
					case 6:
						processedText = "Does talking about this bother you ?";
						break;
						
					default:
						break;
					}
					System.out.println("New text: " + tweetStatus + " >>> "  + processedText);
					TwitterUtilities.commentRetweet(status.getId(), processedText, twitterHandle, twitterAccount);
				}
			}
		}
		
	}



	/***
	 * 
	 */
	private static void RespondToMentions() {

		ResponseList<Status> mentions = TwitterUtilities.getMentions(twitterAccount);
		for(Status status : mentions)
		{
			Calendar cCreated = Calendar.getInstance();
			cCreated.setTime(status.getCreatedAt());
			cCreated.add(Calendar.HOUR, hoursBetweenRuns); // check if any new tweets in the last hour
			if (elizaAlive && cCreated.after(cCurrent))
			{
				String processedText = elizaMain.processInput(status.getText());
				System.out.println(status.getText() + " >>> "  + processedText);
				TwitterUtilities.sendReply(status.getId(), processedText, status.getUser().getScreenName(), twitterAccount);
			}
		}
		
	}

	/***
	 * 
	 */
	private static void respondToDMs() {
		ResponseList<DirectMessage> dms = TwitterUtilities.getDirectMessages(twitterAccount);
		for(DirectMessage dm : dms)
		{
			Calendar cCreated = Calendar.getInstance();
			cCreated.setTime(dm.getCreatedAt());
			cCreated.add(Calendar.HOUR, 1); // check if any new tweets in the last hour
			if (elizaAlive && cCreated.after(cCurrent))
			{
				String processedText = elizaMain.processInput(dm.getText());
				System.out.println(dm.getText() + " >>> "  + processedText);
				TwitterUtilities.sendDM(dm.getSender(), processedText, twitterAccount);
			}
		}
		
	}

}
