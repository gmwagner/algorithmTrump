package rss;

import java.util.ArrayList;
import java.util.List;



// Class to store feed model
public class Feed {
	  private String title;
	  private String link;
	  private String description;
	  private String language;
	  private String copyright;
	  private String pubDate;

	  private List<FeedMessage> entries = new ArrayList<FeedMessage>();

	  public Feed(String title, String link, String description, String language,
	      String copyright, String pubDate) {
	    this.title = title;
	    this.link = link;
	    this.description = description;
	    this.language = language;
	    this.copyright = copyright;
	    this.pubDate = pubDate;
	  }

	  public List<FeedMessage> getMessages() {
	    return entries;
	  }

	  public String getTitle() {
	    return title;
	  }

	  public String getLink() {
	    return link;
	  }

	  public String getDescription() {
	    return description;
	  }

	  public String getLanguage() {
	    return language;
	  }

	  public String getCopyright() {
	    return copyright;
	  }

	  public String getPubDate() {
	    return pubDate;
	  }

	  @Override
	  public String toString() {
	    return "Feed [copyright=" + copyright + ", description=" + description
	        + ", language=" + language + ", link=" + link + ", pubDate="
	        + pubDate + ", title=" + title + "]";
	  }
	  
	  public String outChannelData()
	  {
		  return "<title><![CDATA["+ getTitle()+ " ]]></title>"+ 
				"<description>"+getDescription()+"</description>\n" + 
		  		"<link>"+getLink()+"</link>\n" + 
		  		"<ttl>30</ttl>\n" + 
		  		"<language>en-us</language>\n" + 
		  		"<generator>Snoozle Sports "+getLink()+"</generator>\n" + 
		  		"<copyright>"+getCopyright()+"</copyright>\n" + 
		  		"<lastBuildDate>"+getPubDate()+"</lastBuildDate>\n" + 
		  		"<managingEditor>admin@snoozle.net (Snoozle)</managingEditor>";
	  }
}
