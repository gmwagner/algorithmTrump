package rss;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import snoozle.algorithmTrump.SentimentString;

public class ReadTest {
	private final static SimpleDateFormat sdfPubDate = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
	
	public static void main(String[] args) throws ParseException {
		
		String searchTerm = "Trump";
	    //Connection con = DBUtilities.connectDB();
	    ArrayList<FeedDB> feeds = new ArrayList<FeedDB>();
	    //FeedDatabaseAPI.retrieveAll(con, feeds);
	    feeds.add(new FeedDB("http://rss.cnn.com/rss/cnn_allpolitics.rss"));
	    ArrayList<SentimentString> sentiments = new ArrayList<SentimentString>();
	    for (FeedDB feedDB : feeds)
	    {
	    	RSSFeedParser parser = new RSSFeedParser(feedDB.getFeed());
	    	//int category = feedDB.getCategory();
	    	
	    	Feed feed = parser.readFeed();
	    	String[] titleSplit = feed.getDescription().split(" ");
	    	String source = "";
	    	if(titleSplit.length > 0)
	    	{
	    		source = titleSplit[0];
	    	}
	    	for (FeedMessage message : feed.getMessages()) {
	    		if(message.getDescription().contains(searchTerm))
	    		{
			    	SentimentString sentimentString = new SentimentString(message.getDescription(),
			    			sdfPubDate.parse(feed.getPubDate()),source, message.getLink());
			    	sentiments.add(sentimentString);
	    		}
		    	//NewsDatabaseAPI.addStory(con, newsInfo);
		    }
	    }

	}
}
