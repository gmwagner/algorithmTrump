package rss;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FeedDatabaseAPI {
	
	// inserts feed into db
	public static void addFeed(Connection con, FeedDB feedDB)
	{
		String insertStatement = "INSERT INTO "
				+ "rssfeeds(feed,category)"
				+ " VALUES(?,?)";
		PreparedStatement pstmt = null;
		try
		{
			pstmt = con.prepareStatement(insertStatement);
			pstmt.setString(1, feedDB.getFeed());
			pstmt.setInt(2, feedDB.getCategory());
			pstmt.executeUpdate();
			
		}
		catch (SQLException ex) {
	         Logger lgr = Logger.getLogger(FeedDatabaseAPI.class.getName());
	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
		finally
		{
			try{
	        	if(pstmt != null)
	        	{
	        		pstmt.close();
	        	}
        	}
    	    catch (SQLException ex) {
    	         Logger lgr = Logger.getLogger(FeedDatabaseAPI.class.getName());
    	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
    		}
		}
	}
	
	public static void retrieveAll(Connection con, ArrayList<FeedDB> feeds)
	{
		String queryStatement = "SELECT * FROM rssfeeds";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			
			pstmt = con.prepareStatement(queryStatement);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				feeds.add(new FeedDB(rs.getString(2), rs.getInt(3)));
			}
			
		}
		catch (SQLException ex) {
	         Logger lgr = Logger.getLogger(FeedDatabaseAPI.class.getName());
	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
	    finally
	    {
	        	
	       	try{
		        	if(pstmt != null)
		        	{
		        		pstmt.close();
		        	}
		        	if(rs != null)
		        	{
		        		rs.close();
		        	}
	       	}
	   	    catch (SQLException ex) {
	   	         Logger lgr = Logger.getLogger(FeedDatabaseAPI.class.getName());
	   	         lgr.log(Level.SEVERE, ex.getMessage(), ex);
	   		}
	    }
	}
}
