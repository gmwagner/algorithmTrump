package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import opennlp.tools.parser.ParserModel;
import opennlp.tools.sentdetect.SentenceModel;

public class NLPTools {
	
	public static final String OPEN_NLP_MODEL_DIR = "/home/greg/workspace/algorithmTrump/src/main/resources/openNLP_model/";
	
	
	public static ParserModel getParseModel() throws FileNotFoundException
	{
		ParserModel model = null;
		InputStream modelIn;
		
		modelIn = new FileInputStream(OPEN_NLP_MODEL_DIR+"en-parser-chunking.bin");
		
		try {
		  model = new ParserModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
		
		return model;
	}
	
	/***
	 * 
	 * @return
	 * @throws FileNotFoundException
	 */
	public static SentenceModel getSentenceModel() throws FileNotFoundException{
		InputStream modelIn = new FileInputStream(utilities.NLPTools.OPEN_NLP_MODEL_DIR+"en-sent.bin");
		SentenceModel model = null;
		try {
		  model = new SentenceModel(modelIn);
		}
		catch (IOException e) {
		  e.printStackTrace();
		  System.out.println("CLOSING: Could not open NLP sentence model");
		}
		finally {
		  if (modelIn != null) {
		    try {
		      modelIn.close();
		    }
		    catch (IOException e) {
		    }
		  }
		}
		
		return model;
	}

}
