package utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import twitter.DatabaseAPI;
import twitter.TwitterNetworkDAO;
import twitter4j.DirectMessage;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import utilities.TwitterUtilities.TwitterAccounts;

public class TwitterUtilities {

	static private String consumerKeyStr;
	static private String consumerSecretStr;
	static private String accessTokenStr; 
	static private String accessTokenSecretStr; 
	
	static private String consumerKeyStrAlgoTrump = "Jz2VQ4htqSpobmk1fnbZ3oO0C";
	static private String consumerSecretStrAlgoTrump = "cvMmpxksu39fIfDOprp6knYOiMrMPg9f6T7JZX5TkXpTYsmMGe";
	static private String accessTokenStrAlgoTrump = "838403572282765313-WZJrfR1il38Boxvyi9GFDNQR2f7aLL5"; 
	static private String accessTokenSecretStrAlgoTrump = "78veI6iEHDg43nmyih8b63zXClyt95GVrvOlkUHJBxM8A"; 
	
	static private String consumerKeyStrELIZA = "hnp7xy57vNkXQoMT0QVh1OvE6";
	static private String consumerSecretStrELIZA = "kco14rFEoWqi8dRJCFnq4nJJxbw4VCKe0u7WoLHFQhO11pkDlR";
	static private String accessTokenStrELIZA = "840582068631302145-UE02PTCaIzsRvdaodTcv7zXvjPFNp03"; 
	static private String accessTokenSecretStrELIZA = "8IiCD1vVxpTXitEPmzmigRTEa0xha0XFPLdx00HENFAiU"; 
	
	static final private String twitterHome = "https://twitter.com";
	

	private static final int numOfTweetsPerPage = 50;
	public static final double botScoreThreshold = 0.50;
	
	
	// enum
	public enum TwitterAccounts
	{
		ALGORITHM_TRUMP(1),
		ELIZA(2);
		
		private int num;
		
		TwitterAccounts(int num)
		{
			this.num = num;
		}
		
		public int getNum()
		{
			return this.num;
		}
	}
	
	public static ResponseList<Status> getMentions(TwitterAccounts twitterAccount)
	{
		Twitter twitter = getTwitterSingleton(twitterAccount);
		ResponseList<Status> mentions = null;
		try {
			mentions = twitter.getMentionsTimeline();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // get's last 20 mentions
		return mentions;
	}
	
	// Basic twitter message sender
	public static void tweetMessage(String mess, TwitterAccounts twitterAccount)
	{
		Twitter twitter = getTwitterSingleton(twitterAccount);
		try {
			Status status = twitter.updateStatus(mess);
			System.out.println("Successfully updated the status to [" + status.getText() + "].");
		} catch (TwitterException e) {
			System.out.println("Failed to send message: " + mess);
			e.printStackTrace();
		}
		
	
	} // public static void tweetMessage(String mess)
	
	// Get messages from user returns 20
	public static ResponseList<Status> getFromUser(String handle, TwitterAccounts twitterAccount)
	{
		int numOfTweets = 20;
		return getFromUser(handle,numOfTweets, twitterAccount);
		
	}
	
	// Get messages from user with paging
	public static ResponseList<Status> getFromUser(String handle, int numberOfTweets, TwitterAccounts twitterAccount)
	{
		Twitter twitter = getTwitterSingleton(twitterAccount);
		ResponseList<Status> responses = null;
		try {
			Paging paging = new Paging(1,numberOfTweets);
			responses = twitter.getUserTimeline(handle, paging);
		} catch (TwitterException e) {
			System.out.println("Error with getting username: " + handle);
			e.printStackTrace();
		}
		return responses;
		
	}
	
	// Get messages from user with paging
	public static ResponseList<Status> getFromUser(Long id, int numberOfTweets, TwitterAccounts twitterAccount)
	{
		Twitter twitter = getTwitterSingleton(twitterAccount);
		ResponseList<Status> responses = null;
		try {
			Paging paging = new Paging(1,numberOfTweets);
			responses = twitter.getUserTimeline(id, paging);
		} catch (TwitterException e) {
			System.out.println("Error with getting id: " + id);
			e.printStackTrace();
		}
		return responses;
		
	}
	
	// Retweet 
	public static void commentRetweet(long statusId, String mess, String handle, TwitterAccounts twitterAccount)
	{
		Twitter twitter = getTwitterSingleton(twitterAccount);
		String tweet = String.format("%s %s/%s/status/%d", mess,twitterHome,handle,statusId);
		StatusUpdate status = new StatusUpdate(tweet);
		
		try {
			Status statVal = twitter.updateStatus(status);
			System.out.println("Successfully updated the status to [" + statVal.getText() + "].");
		} catch (TwitterException e) {
			System.out.println("Failed to send message: " + mess);
			e.printStackTrace();
		}
	}
	
	// Reply
	public static void sendReply(long statusId, String mess, TwitterAccounts twitterAccount)
	{
		sendReply(statusId, mess, "", twitterAccount);
	}
	
	public static void sendReply(long statusId, String mess, String handle, TwitterAccounts twitterAccount)
	{
		if(!handle.isEmpty())
		{
			handle = "@" + handle;
		}
		Twitter twitter = getTwitterSingleton(twitterAccount);
		StatusUpdate status = new StatusUpdate(handle + " " + mess);
		status.setInReplyToStatusId(statusId);
		
		try {
			Status statVal = twitter.updateStatus(status);
			System.out.println("Successfully updated the status to [" + statVal.getText() + "].");
		} catch (TwitterException e) {
			System.out.println("Failed to send message: " + mess);
			e.printStackTrace();
		}
	}
	
	// Get Twitter Singleton
	public static Twitter getTwitterSingleton(TwitterAccounts twitterAccount)
	{
		setAccount(twitterAccount);
		
		try {
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true);
			cb.setJSONStoreEnabled(true);
			cb.setTweetModeExtended(true);
			cb.setOAuthConsumerKey(consumerKeyStr);
			cb.setOAuthConsumerSecret(consumerSecretStr);
			cb.setOAuthAccessToken(accessTokenStr);
			cb.setOAuthAccessTokenSecret(accessTokenSecretStr);
			
			TwitterFactory tf = new TwitterFactory(cb.build());
			Twitter twitter = tf.getInstance();
			//Twitter twitter = new TwitterFactory().getInstance();
			try {
				// get request token.
				// this will throw IllegalStateException if access token is already available
				RequestToken requestToken = twitter.getOAuthRequestToken();
				System.out.println("Got request token.");
				System.out.println("Request token: " + requestToken.getToken());
				System.out.println("Request token secret: " + requestToken.getTokenSecret());
				AccessToken accessToken = null;
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				while (null == accessToken) {
					System.out.println("Open the following URL and grant access to your account:");
					System.out.println(requestToken.getAuthorizationURL());
					System.out.print("Enter the PIN(if available) and hit enter after you granted access.[PIN]:");
					String pin = br.readLine();
					try {
						if (pin.length() > 0) {
							accessToken = twitter.getOAuthAccessToken(requestToken, pin);
						} else {
							accessToken = twitter.getOAuthAccessToken(requestToken);
						}
					} catch (TwitterException te) {
						if (401 == te.getStatusCode()) {
								System.out.println("Unable to get the access token.");
							} 
						else {
									te.printStackTrace();
								}
					}
				}
				System.out.println("Got access token.");
				System.out.println("Access token: " + accessToken.getToken());
				System.out.println("Access token secret: " + accessToken.getTokenSecret());
			} catch (IllegalStateException ie) {
			// access token is already available, or consumer key/secret is not set.
				if (!twitter.getAuthorization().isEnabled()) {
					System.out.println("OAuth consumer key/secret is not set.");
					System.exit(-1);
				}
			}
		return twitter;
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to get timeline: " + te.getMessage());
			System.exit(-1);
		} catch (IOException ioe) {
			ioe.printStackTrace();
			System.out.println("Failed to read the system input.");
			System.exit(-1);
		}
		return null;
	} // public static Twitter getTwitterSingleton()

	private static void setAccount(TwitterAccounts twitterAccount) {
		switch (twitterAccount.getNum())
		{
		case 1: // algorithm Trump
			consumerKeyStr = consumerKeyStrAlgoTrump;
			consumerSecretStr =  consumerSecretStrAlgoTrump;
			accessTokenStr = accessTokenStrAlgoTrump; 
			accessTokenSecretStr = accessTokenSecretStrAlgoTrump; 
			break;
		case 2:
			consumerKeyStr = consumerKeyStrELIZA;
			consumerSecretStr =  consumerSecretStrELIZA;
			accessTokenStr = accessTokenStrELIZA; 
			accessTokenSecretStr = accessTokenSecretStrELIZA; 
			break;
			
		}
		
	}

	/***
	 * 
	 * @param twitterAccount
	 * @return
	 */
	public static ResponseList<DirectMessage> getDirectMessages(TwitterAccounts twitterAccount) {
		Twitter twitter = getTwitterSingleton(twitterAccount);
		ResponseList<DirectMessage> dm = null;
		try {
			dm = twitter.getDirectMessages();
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // get's last 20 mentions
		return dm;
	}

	/**
	 * 
	 * @param user
	 * @param mess
	 * @param twitterAccount
	 */
	public static void sendDM(User user, String mess, TwitterAccounts twitterAccount) {
		Twitter twitter = getTwitterSingleton(twitterAccount);
		DirectMessage dm = null;
		try {
			dm = twitter.sendDirectMessage(user.getId(), mess);
			System.out.println("Sent direct message to " + user.getScreenName() + " [" + dm.getText() + "].");
		} catch (TwitterException e) {
			System.out.println("Failed to send message to " + user.getScreenName() + ": " + mess);
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @param twitter
	 * @param family
	 * @param key
	 */	
	public static void rateThrottle(RateLimitStatus rls)
	{
		if(rls.getRemaining() <= 0)
		{
			try {
				TimeUnit.SECONDS.sleep(rls.getSecondsUntilReset());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/***
	 * 
	 * @param screenName
	 * @param twitter
	 * @param numOfTweets 
	 * @return
	 */
	public static ArrayList<Status> getTweetsForUser(long id, Twitter twitter, int numOfTweets) {
		ArrayList<Status> statuses = new ArrayList<Status>();
		int pageSize = 50;
		int numberOfPages = (int) Math.ceil((double)numOfTweets/(double)pageSize);
		Paging paging = new Paging();
		paging.setCount(pageSize);
		try {
			for(int counter = 1; counter <= numberOfPages; counter++)
			{
				paging.setPage(counter);
				//statuses/user_timeline
				ResponseList<Status> responses = twitter.getUserTimeline(id,paging);
				statuses.addAll(responses);
			}
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return statuses;
	}
		
	/**
	 * 
	 * @param statuses
	 * @return
	 */
	public static String makeTweetObject(ArrayList<Status> statuses) {
		//[{\"id\":1234567890,\"text\":\"@Botometer is so cool!\",\"...\":\"...\"},\"...\"]
		
		String returnString = "[";
		for(Status status: statuses)
		{
	        //String statusJson = gson.toJson( status );
			String statusJson = TwitterObjectFactory.getRawJSON(status); // status to json
			if(statusJson != null)
			{
				returnString += statusJson+",";
			}
		}
		returnString = returnString.substring(0, returnString.length()-1);
		returnString += "]";
		return returnString;
	}

	/**
	 * 
	 * @param screenName
	 * @param twitter
	 * @param numoftweetsperpage
	 * @return
	 */
	public static ArrayList<Status> getMentions(String screenName, Twitter twitter, int numoftweetsperpage) {
		ArrayList<Status> mentions = new ArrayList<Status>();
		
		Query query = new Query("@"+screenName);
		QueryResult result;
		try {

			///search/tweets
			result = twitter.search(query);
			mentions.addAll(result.getTweets());
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mentions;
	}

	/**
	 * 
	 * @param allowBots 
	 * @param followId
	 */
	public static void addFollower(Twitter twitter, Long id, boolean allowBots) {
		
		TwitterNetworkDAO twitterNet = new TwitterNetworkDAO();
		twitterNet.setTwitterID(id);
		twitterNet.setFriend(true);
		twitterNet.setFriendDate(new Date());
		
		double botScore;
		try {
			botScore = TwitterUtilities.botOrNot(id,twitter);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			botScore = 1.0;
		} catch (TwitterException e) {
			e.printStackTrace();
			botScore = 1.0;
		}
		boolean isBot = botScore >= TwitterUtilities.botScoreThreshold;
		if(isBot && !allowBots)
		{
			return;
		}
		if(isBot)
		{
			twitterNet.setBot(true);
			twitterNet.setBotDate(new Date());
		}
		
		System.out.println("Adding user: " + id);
		try {
			DatabaseAPI.updateInfo(twitter.getScreenName(), twitterNet);
		} catch (IllegalStateException | TwitterException e) {
			e.printStackTrace();
		}
		
		
		try {
			twitter.createFriendship(id);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
			
		
		
	}
	
	/**
	 * 
	 * @param id
	 * @param twitter
	 * @return
	 * @throws IllegalStateException
	 * @throws TwitterException
	 */
	public static double botOrNot(long id, Twitter twitter) throws IllegalStateException, TwitterException
	{
		Map<String, RateLimitStatus> rls = twitter.getRateLimitStatus();
		RateLimitStatus metaRLS = rls.get("/application/rate_limit_status");
		TwitterUtilities.rateThrottle(metaRLS);
		
		RateLimitStatus statusRLS = rls.get("/statuses/user_timeline");
		TwitterUtilities.rateThrottle(statusRLS);
		ArrayList<Status> statuses = TwitterUtilities.getTweetsForUser(id,twitter,numOfTweetsPerPage);
		if(statuses.size() == 0)
		{
			// if no statuses declare a bot
			return 1.0;
		}
		String screenName = statuses.get(0).getUser().getScreenName();
		String userObject = "{\"id\":\""+id+"\",\"screen_name\":\""+screenName+"\"}";
		String timeLineObject = TwitterUtilities.makeTweetObject(statuses);
		if(timeLineObject.equals("]"))
		{
			// if all junk statuses declare as bot
			timeLineObject = "[]";
		}
		
		RateLimitStatus mentionRLS = rls.get("/search/tweets");
		TwitterUtilities.rateThrottle(mentionRLS);
		ArrayList<Status> mentions = TwitterUtilities.getMentions(screenName,twitter,numOfTweetsPerPage);
		String mentionObject = TwitterUtilities.makeTweetObject(mentions);
		if(mentionObject.equals("]"))
		{
			// if all junk statuses declare as bot
			mentionObject = "[]";
		}
		
		String fullObject = "{\"user\":"+userObject+",\"timeline\":"+timeLineObject+",\"mentions\":"+mentionObject+"}";
		double score = 1.0;
		try
		{
			HttpResponse<JsonNode> response = Unirest.post("https://osome-botometer.p.mashape.com/2/check_account")
			.header("X-Mashape-Key", "kc1YoqbsN9mshmcDwfiVbDaJlPSop16alWnjsnbFoWeo5NeeMA")
			.header("X-Mashape-Host", "osome-botometer.p.mashape.com")
			.header("Content-Type", "application/json")
			.header("Accept", "application/json")
			.body(fullObject)
			.asJson();
			
			score = getScore(response);
		}
		catch( UnirestException e )
		{
			System.out.println("Problem with user: " + screenName);
			e.printStackTrace();
		}
		
		return score;
	}

	/***
	 * 
	 * @param response
	 */
	private static double getScore(HttpResponse<JsonNode> response) {
		Double score = 1.0;
		try
		{
			JsonElement jelement = new JsonParser().parse(response.getBody().toString());
			JsonObject  jobject = jelement.getAsJsonObject();
			jobject = jobject.getAsJsonObject("scores");
			score = Double.valueOf(jobject.get("english").toString());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Returning 1.0 as the score");
		}
		
		return score;
		
	}
}
