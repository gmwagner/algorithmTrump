package utilities;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBUtilities {
	
	//=================================
	// connectDB
	//
	// Connects to the database
	//
	//=================================
	public static Connection connectDB() {
		Connection con = null;
       
        try {
        	//Class.forName("com.mysql.jdbc.Driver");
        	URI dbUri = new URI(System.getenv("DATABASE_URL_AWS"));

            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
        	//DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            con = DriverManager.getConnection(dbUrl, username, password);
            
        }
        catch (SQLException ex) {
            Logger lgr = Logger.getLogger(DBUtilities.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } 
        catch (URISyntaxException ex) {
            Logger lgr = Logger.getLogger(DBUtilities.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
		} 
        
        return con;
	}
	

	//=========================================
	//
	// closeDB
	//
	// closes the DB
	//
	//=========================================
	public static void closeDB(Connection con) {
		try {
            if (con != null) {
                con.close();
            }

        } 
        catch (SQLException ex) {
            Logger lgr = Logger.getLogger(DBUtilities.class.getName());
            lgr.log(Level.WARNING, ex.getMessage(), ex);
        }
		
	}

}
