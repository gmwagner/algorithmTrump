package utilities;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class Crawl {
	
	// gets the doc class for crawl
	public static Document getDocument(String url)
	{
		Document doc = null;
		boolean openDoc = false;
		int tries = 0;
		final int maxTries = 3;
		do{
			try
			{
				Connection conn = Jsoup.connect(url).timeout(10000);
				Response response = conn.execute();
				int statusCode = response.statusCode();
				if(statusCode == 200)
				{
					doc = conn.get();
					openDoc = true;
				}
				else
				{
					System.out.print("Got a " + statusCode + " code for " + url);
				}
			}
			catch(SocketTimeoutException e)
			{
				System.out.println("Time out exception for " + url + " Try " + tries);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			} catch (IOException e) {
				System.out.println("Error crawler: Could not access " + url);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			}
		}while(!openDoc);
		
		return doc;
	}
	
	public static Document getDocument(String url, HashMap<String,String> data)
	{
		Document doc = null;
		boolean openDoc = false;
		int tries = 0;
		final int maxTries = 3;
		do{
			try
			{
				Connection conn = Jsoup.connect(url).ignoreContentType(true).timeout(10000);
				for(String key : data.keySet())
				{
					conn = conn.data(key,data.get(key));
				}
				Response response = conn.execute();
				int statusCode = response.statusCode();
				if(statusCode == 200)
				{
					doc = conn.post();
					openDoc = true;
				}
				else
				{
					System.out.print("Got a " + statusCode + " code for " + url);
				}
			}
			catch(SocketTimeoutException e)
			{
				System.out.println("Time out exception for " + url + " Try " + tries);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			} catch (IOException e) {
				System.out.println("Error crawler: Could not access " + url);
				if(tries >= maxTries)
				{
					return null;
				}
				else
				{
					tries++;
				}
			}
		}while(!openDoc);
		
		return doc;
	}
	
}
