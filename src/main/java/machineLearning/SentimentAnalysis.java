package machineLearning;

import com.datumbox.framework.applications.nlp.TextClassifier;
import com.datumbox.framework.common.Configuration;
import com.datumbox.framework.core.common.dataobjects.Record;
import com.datumbox.framework.common.utilities.RandomGenerator;
import com.datumbox.framework.core.machinelearning.MLBuilder;
import com.datumbox.framework.core.machinelearning.classification.MultinomialNaiveBayes;
import com.datumbox.framework.core.machinelearning.modelselection.metrics.ClassificationMetrics;

import snoozle.algorithmTrump.SentimentString;

import com.datumbox.framework.core.common.text.extractors.NgramsExtractor;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SentimentAnalysis {
	
	private final static String resourceArea = "/home/greg/workspace/algorithmTrump/src/main/resources/";
	
	public static SentimentString getSentiment(String message)
	{
		ArrayList<SentimentString> responses = new ArrayList<SentimentString>();
		responses.add(new SentimentString(message));
		responses = getSentiment(responses);
		return responses.get(0);
	}
	/***
	 * 
	 * @param responses
	 * @return
	 */
	public static ArrayList<SentimentString> getSentiment(ArrayList<SentimentString> responses)
	{
		boolean isSilent = true;
		//Initialization
	    //--------------
		ArrayList<SentimentString> sentiments = new ArrayList<SentimentString>();
	    RandomGenerator.setGlobalSeed(42L); //optionally set a specific seed for all Random objects
	    Configuration configuration = Configuration.getConfiguration(); //default configuration based on properties file
	    //configuration.setStorageConfiguration(new InMemoryConfiguration()); //use In-Memory engine (default)
	    //configuration.setStorageConfiguration(new MapDBConfiguration()); //use MapDB engine
	    //configuration.getConcurrencyConfiguration().setParallelized(true); //turn on/off the parallelization
	    //configuration.getConcurrencyConfiguration().setMaxNumberOfThreadsPerTask(4); //set the concurrency level
	    
	    
	    
	    //Reading Data
	    //------------
	    Map<Object, URI> datasets = new HashMap<Object, URI>(); //The examples of each category are stored on the same file, one example per row.
	    try {
	    	datasets.put("positive", new URI("file:"+resourceArea+"datasets/sentiment-analysis/rt-polarity.pos"));
	        datasets.put("negative", new URI("file:"+resourceArea+"datasets/sentiment-analysis/rt-polarity.neg"));
		} catch (URISyntaxException e) {
			System.out.println("Malformed URI");
			e.printStackTrace();
			return null;
		}
	    
	    
	    
	    
	    //Setup Training Parameters
	    //-------------------------
	    TextClassifier.TrainingParameters trainingParameters = new TextClassifier.TrainingParameters();
	
	    //numerical scaling configuration
	    trainingParameters.setNumericalScalerTrainingParameters(null);
	
	    //categorical encoding configuration
	    trainingParameters.setCategoricalEncoderTrainingParameters(null);
	    
	    //Set feature selection configuration
	    //trainingParameters.setFeatureSelectorTrainingParametersList(Arrays.asList(new ChisquareSelect.TrainingParameters()));
	    
	    //Set text extraction configuration
	    trainingParameters.setTextExtractorParameters(new NgramsExtractor.Parameters());
	
	    //Classifier configuration
	    trainingParameters.setModelerTrainingParameters(new MultinomialNaiveBayes.TrainingParameters());
	    
	    
	    
	    //Fit the classifier
	    //------------------
	    TextClassifier textClassifier = MLBuilder.create(trainingParameters, configuration);
	    textClassifier.fit(datasets);
	    textClassifier.save("SentimentAnalysis");
	    
	    
	    
	    //Use the classifier
	    //------------------
	    
	    //Get validation metrics on the dataset
	    ClassificationMetrics vm = textClassifier.validate(datasets);
	    
	    //Classify a single sentence
	    for(SentimentString sentimentString : responses)
	    {
	    	String sentence = sentimentString.getString();
	    	Record r = textClassifier.predict(sentence);
	    	if(!isSilent)
	    	{
		    	System.out.println("Classifing sentence: \""+sentence+"\"");
			    System.out.println("Predicted class: "+r.getYPredicted());
			    System.out.println("Probability: "+r.getYPredictedProbabilities().get(r.getYPredicted()));
			    System.out.println("Classifier Accuracy: "+vm.getAccuracy());
	    	}
		    sentimentString.setSentiment(r.getYPredicted().toString());
			sentimentString.setProbablity((Double) r.getYPredictedProbabilities().get(r.getYPredicted()));
			sentimentString.setClassAccuracy(vm.getAccuracy());
			sentiments.add(sentimentString);
	    }
	    
	    
	    
	    
	    
	    //Clean up
	    //--------
	    
	    //Delete the classifier. This removes all files.
	    textClassifier.delete();
	    
	    return sentiments;
	}// getSentiment
}
